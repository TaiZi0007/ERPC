#include <stdio.h>

#include "erpc.h"

int hello_encrypt_t(char *data, size_t len, int sockfd, rpc_message_handler_t handler)
{
    size_t i = 0;

    for(; i < len; i++)
        data[i] += 3;

    handler(sockfd, data, len);

    return 0;
}

int hello_decrypt_t(char *data, size_t len, int sockfd, rpc_message_handler_t handler)
{
    size_t i = 0;

    for(; i < len; i++)
        data[i] -= 3;

    handler(sockfd, data, len);

    return 0;
}

